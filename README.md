# README #

## Basic Usage: ##

* Initialize a new instance of the _PWMFader_ class with the PWM pins you want to use, 
* call _update()_ to update the RGB values, 
* call _fadeTo(R, G, B, Milliseconds)_ to fade to a specific color

### Example: ###

```
#!arduino

#include <PWMFader.h>

//Config

//PWM RGB Fader on pins 9, 10, 11
PWMFader fader = PWMFader(9, 10, 11);

//End Config

void setup() {
  fader.fadeTo(255, 255, 255, 5000) // fade from black (off) to white (full brightness) in 5 seconds
}

void loop() {
  fader.update();
}
```

## Additional Features: ##

* use _isFading()_ to get if a fader is currently fading
* do inversion by initializing fader with an additional parameter (e.g. _PWMFader fader = PWMFader(9, 10, 11, 1, 1, 1, **true**);_ to invert)
* do color correction by initializing a fader with additional correction parameters (e.g. _PWMFader fader = PWMFader(9, 10, 11, **1, 0.5, 0.5**);_ if red is half as bright as the other colors)

## Advanced Usage: ##

* use other types of output (like WS2811 LEDs) by deriving a new class from the _RGBFader_ class and overriding the _output()_ method