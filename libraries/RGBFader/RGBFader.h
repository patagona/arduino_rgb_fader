#include <stdlib.h>
#include <stdbool.h>

class RGBFader {
private:
	uint8_t _ROld;
	uint8_t _GOld;
	uint8_t _BOld;

	uint8_t _RNew;
	uint8_t _GNew;
	uint8_t _BNew;

	uint8_t _currentR;
	uint8_t _currentG;
	uint8_t _currentB;

	float _corrR;
	float _corrG;
	float _corrB;

	unsigned long _fadeTimeStart;
	unsigned int _fadeTime;
	
	bool _invert;

	bool _fading;

	virtual void output(uint8_t R, uint8_t G, uint8_t B) = 0;
public:
	RGBFader(float corrR = 1, float corrG = 1, float corrB = 1, bool invert = false);
	void update();
	void fadeTo(uint8_t R, uint8_t G, uint8_t B, unsigned int fadeTime);
	bool isFading();
};
