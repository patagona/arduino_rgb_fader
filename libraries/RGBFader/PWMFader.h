#include <stdlib.h>
#include <stdbool.h>
#include <Arduino.h>
#include "RGBFader.h"

class PWMFader : public RGBFader {
private:
	int _RPin;
	int _GPin;
	int _BPin;

	void output(uint8_t R, uint8_t G, uint8_t B){
		analogWrite(_RPin, R);
		analogWrite(_GPin, G);
		analogWrite(_BPin, B);
	}
public:
	PWMFader(int RPin, int GPin, int BPin, float corrR = 1, float corrG = 1, float corrB = 1, bool invert = false) 
  : RGBFader(corrR, corrG, corrB, invert) {
		_RPin = RPin;
		_GPin = GPin;
		_BPin = BPin;
	}
};
