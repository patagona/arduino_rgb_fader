#include <stdbool.h>
#include <Arduino.h>
#include "RGBFader.h"

RGBFader::RGBFader(float corrR, float corrG, float corrB, bool invert) {
	_corrR = corrR;
	_corrG = corrG;
	_corrB = corrB;
	
	_invert = invert;
}

void RGBFader::fadeTo(uint8_t R, uint8_t G, uint8_t B, unsigned int fadeTime) {
	_fadeTimeStart = millis();
  
	_fadeTime = fadeTime;
  
	_ROld = _currentR;
	_GOld = _currentG;
	_BOld = _currentB;

	_RNew = R;
	_GNew = G;
	_BNew = B;

	_fading = true;
	update();
}

void RGBFader::update() {
	if (_fading){
		float x = (float(millis() - _fadeTimeStart)) / float(_fadeTime);

		if (x > 1)
			x = 1;

		float p = (sin((x*PI) - PI / 2) + 1) / 2; //sine
		//float p = 1/(1+exp(-(x*10-5))); //sigmoid function
		//float p = x; //linear
		//float p = pow(x, 2); // squared
		//float p = sqrt(x); // square root

		uint8_t _currentR = round((_RNew * p) + (_ROld * (1 - p)));
		uint8_t _currentG = round((_GNew * p) + (_GOld * (1 - p)));
		uint8_t _currentB = round((_BNew * p) + (_BOld * (1 - p)));

		output(
			_invert ? 255 - (_currentR * _corrR) : (_currentR * _corrR), 
			_invert ? 255 - (_currentG * _corrG) : (_currentG * _corrG), 
			_invert ? 255 - (_currentB * _corrB) : (_currentB * _corrB));

		if (x == 1){
			_fading = false;
		}
	}
}

bool RGBFader::isFading() {
	return _fading;
}
