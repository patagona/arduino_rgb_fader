#include <PWMFader.h>

//Config

//PWM Fader on pins 9, 10, 11
PWMFader f(9, 10, 11);

//End Config

byte color = 0;

void setup() {
}

void loop() {
  f.update();

  if (!f.isFading()) {
    switch (color) {
      case 0:
        f.fadeTo(255, 0, 0, 3000);
        color++;
        break;
      case 1:
        f.fadeTo(255, 255, 0, 3000);
        color++;
        break;
      case 2:
        f.fadeTo(0, 255, 0, 3000);
        color++;
        break;
      case 3:
        f.fadeTo(0, 255, 255, 3000);
        color++;
        break;
      case 4:
        f.fadeTo(255, 0, 255, 3000);
        color++;
        break;
      default:
        color = 0;
        break;
    }
  }
}
